<?php

Route::group(['middleware' => ['logRegister','verifyParam']], function(){
	Route::get('V1/stores/{latitude?}/{longitude?}','SistemaController@getStores');
});