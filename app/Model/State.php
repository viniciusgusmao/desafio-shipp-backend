<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    //
    protected $table = "state";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function city()
    {
        return $this->hasMany('App\Model\City','state_id', 'id');
    }

}
