<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EstablishmentType extends Model
{
    //
    protected $table = "establishmentType";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function store()
    {
        return $this->hasMany('App\Model\Store','establishmentType_id', 'id');
    }
}
