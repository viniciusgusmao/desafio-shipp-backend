<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    //
    protected $table = "store";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function county()
    {
        return $this->belongsTo('App\Model\County', 'county_id');
    }

    public function establishmentType()
    {
        return $this->belongsTo('App\Model\EstablishmentType', 'establishmentType_id');
    }

    public function operationType()
    {
        return $this->belongsTo('App\Model\OperationType', 'operationType_id');
    }
}
