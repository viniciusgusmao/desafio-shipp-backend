<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    //
    protected $table = "county";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function city()
    {
        return $this->belongsTo('App\Model\City', 'city_id');
    }

    public function store()
    {
        return $this->hasMany('App\Model\Store','county_id', 'id');
    }
}
