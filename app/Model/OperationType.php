<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OperationType extends Model
{
    //
    protected $table = "operationType";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function store()
    {
        return $this->hasMany('App\Model\OperationType','operationType_id', 'id');
    }
}
