<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    //
    protected $table = "city";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function state()
    {
        return $this->belongsTo('App\Model\State', 'city_id');
    }

    public function county()
    {
        return $this->hasMany('App\Model\County','city_id', 'id');
    }
}
