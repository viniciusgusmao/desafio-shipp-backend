<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SistemaRepository;

class SistemaController extends Controller
{
    
    public function getStores($latitude,$longitude)
    {	
        $sistema = new SistemaRepository;
        return $sistema->getStores($latitude,$longitude,"listagem");
    }
}
