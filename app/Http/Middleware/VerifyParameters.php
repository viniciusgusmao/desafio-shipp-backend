<?php

namespace App\Http\Middleware;

use Closure;

class VerifyParameters
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path = explode('/',$request->path());
        if (count($path) != 4)
            return response('Bad Request', 400);          
        else {
            $latitude = $path[2];
            $longitude = $path[3];
            // VALIDANDO AS COORDENADAS
            if (!isValidLongitude($longitude) || !isValidLatitude($latitude))
                return response('Not Acceptable', 406); 
                
        }
        return $next($request);
       
    }
}
