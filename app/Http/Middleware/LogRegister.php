<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\SistemaRepository;

class LogRegister
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $sistema = new SistemaRepository;
        $path = explode('/',$request->path());
        $latitude = "";
        $longitude = "";
        $quantidade = 0;
        if (count($path) != 4)
            $status = 400;
        else {            
            $latitude = $path[2];
            $longitude = $path[3];
            // VALIDANDO AS COORDENADAS
            if (!isValidLongitude($longitude) || !isValidLatitude($latitude))
                $status = 406;
            else{
                $status = 200;
                $quantidade = $sistema->getStores($latitude,$longitude,"quantidade");
            }
        }
        $sistema->registrarLog($latitude,$longitude,$status,$quantidade);
        return $next($request);
    }
}
