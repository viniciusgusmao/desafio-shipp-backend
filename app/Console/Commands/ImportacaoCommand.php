<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ImportacaoRepository;
use Cache;

class ImportacaoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importar dados para o banco sqlite';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Cache::flush();
        echo "Aguarde a importacao de dados. Horario de inicio: ".date("H:i:s").PHP_EOL;

        if (!file_exists("database/database.sqlite")): // VERIFICA SE O BANCO ESTÁ CRIADO, SE NAO TIVER, CRIAR E EXECUTA O php artisan migrate
            $database = fopen("database/database.sqlite", "w");
            shell_exec("php artisan migrate");
        endif;

        if (!file_exists(storage_path('app/public/banco.csv'))): // VERIFICA SE O BANCO ESTÁ CRIADO, SE NAO TIVER, CRIAR E EXECUTA O php artisan migrate
            copy(public_path('banco.csv'), storage_path('app/public/banco.csv'));
        endif;

        $importacao = new ImportacaoRepository;
        $dados = file(storage_path('app/public/banco.csv'));
        $header = array_shift($dados);
        
        $importacao->truncateTables();

        $vetState_ = [];
        $vetCity_ = [];
        $vetCounty_ = [];            
        $vetEstablishmentType_ = [];
        $vetOperationType_ = [];
        $vetAux = [];
        // PRIMEIRO LOOP VAI POPULAR ARRAYS PARA INSERIR POSTERIORMENTE NAS TABELAS county, state, city, establishmentType, operationType
        foreach($dados as $d):
            $fields = str_getcsv($d,',','"');

            $fields = array_map(function($n){ return trim($n); }, $fields);

            $vetState_[] = $fields[11];
            $vetCity_[] = array("state" => $fields[11], "city" => $fields[10] );
            $vetCounty_[] = array("city" => $fields[10], "county" => $fields[0] );
            $vetEstablishmentType_[] = $fields[3];
            $vetOperationType_[] = $fields[2]; 
        endforeach;

        // FAZENDO A INSERÇÃO NAS TABELAS county, state, city, establishmentType, operationType E RETORNANDO OS ARRAYS COM O PADRÃO ID => VALUE 
        $vetState = $importacao->cadastrarGenericModel($vetState_,"State");
        $vetCity = $importacao->cadastrarCity($vetState,$vetCity_);
        $vetCounty = $importacao->cadastrarCounty($vetCity,$vetCounty_);
        $vetEstablishmentType = $importacao->cadastrarGenericModel($vetEstablishmentType_,"EstablishmentType");
        $vetOperationType = $importacao->cadastrarGenericModel($vetOperationType_,"OperationType");
       
        // PREPARANDO O SQL DE INSERÇÃO DAS LOJAS PARA REALIZAR POSTERIORMENTE DE UMA SÓ VEZ, UTILIZANDO O ELOQUENT
        $vetCounty_flip = array_flip($vetCounty);
        $vetOperationType_flip = array_flip($vetOperationType);
        $vetEstablishmentType_flip = array_flip($vetEstablishmentType);
        $vetPrepareSql = [];
        $controle = 0;
        $contadorId = 1;
        foreach($dados as $d):
            $controle++;

            $fields = str_getcsv($d,',','"');
            
            $fields = array_map(function($n){ return trim($n); }, $fields);
            
            $str = str_replace('"','',$fields[14]);
            $str = str_replace("'","",$str);
            
            // PEGANDO A LONGITUDE
            $v1 = explode('longitude:',$str);
            if (isset($v1[1])):
                $v2 = explode(',',$v1[1]);
                $longitude = (double) $v2[0];
                // PEGANDO A LATITUDE
                $v3 = explode('latitude:',$v1[1]);
                $latitude = (double) $v3[1];
            else:
                $longitude = "";
                $latitude = "";
            endif;

            // PREPARANDO O SQL PARA SER CADASTRADO, TRATANDO AS CHAVES ESTRANGEIRAS NOS CAMPOS county_id,operationType_id, establishmentType_id
            $vetPrepareSql[] = array(
                "county_id" => $vetCounty_flip[$fields[0]],
                "license_number" => $fields[1],
                "operationType_id" => $vetOperationType_flip[$fields[2]],
                "establishmentType_id" => $vetEstablishmentType_flip[$fields[3]],
                "entity_name" => $fields[4],
                "dba_name" => $fields[5],
                "street_number" => $fields[6],
                "street_name" => $fields[7],
                "address_line_2" => $fields[8],
                "address_line_3" => $fields[9],
                "zipcode" => $fields[12],
                "square_footage" => $fields[13],
                "longitude" => $longitude,
                "latitude"  => $latitude
            );
            // NECESSIDADE DE DIVIDIR A INSERÇÃO DAS LOJAS EM 40 REGISTROS POR VEZ, POIS O SQL FICOU DANDO O ERRO: too many SQL variables
            if ($controle == 40):
                $importacao->cadastrarStore($vetPrepareSql); 
                $vetPrepareSql = [];
                $controle = 0;
            endif;

            $vetAux[$contadorId] = array("entity_name" => $fields[4], "latitude" => $latitude, "longitude" => $longitude );
            $contadorId++;
        endforeach;
        if (count($vetPrepareSql) > 0)
            $importacao->cadastrarStore($vetPrepareSql); 
        
        // REGISTRAR NO CACHE AS LOJAS PARA OTIMIZAR A BUSCA POSTERIORMENTE
        Cache::forever('stores', $vetAux);

        echo "Importacao finalizada com sucesso. Horario fim: ".date("H:i:s").PHP_EOL;
        
        
    }
}
