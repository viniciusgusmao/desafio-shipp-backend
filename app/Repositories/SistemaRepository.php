<?php

namespace App\Repositories;

use Geotools;
use App\Model\Store;
use App\Model\Log;
use App\Model\City;
use Cache;

class SistemaRepository
{
    
    public function getStores($latitude,$longitude,$tipoReq)
    {   
        if ($tipoReq == "quantidade"):
            Cache::forget(md5($latitude.$longitude));
            $stores = Cache::get('stores');
            $vetAux = [];
            $arrayIdLoja = [];
            $coordA   = Geotools::coordinate([$latitude, $longitude]);
            foreach($stores as $id => $info):
                if ($info['longitude'] != '' && $info['latitude'] != '' && $info['entity_name'] != ""):
                    $coordB   = Geotools::coordinate([$info['latitude'], $info['longitude']]);
                    $distance = Geotools::distance()->setFrom($coordA)->setTo($coordB);
                    $vetAux[$id] = array ("entity_name" => $info['entity_name'], "distance" => round($distance->in('km')->haversine(),2));
                endif;
            endforeach;

            // FILTRAR SOMENTE AS LOJAS COM O KM ABAIXO DE 6.5 DE PROXIMIDADE
            $result = array_filter($vetAux,function($v){
                return ($v['distance'] < 6.5);
            });
            $result2_ = array_column($result,"distance","entity_name");
            // ORDENAR O ARRAY DE ACORDO COM A DISTÂNCIA
            asort($result2_); 
            $result2 = [];
            foreach($result2_ as $entity_name => $distance):
                $result2[] = array("store" => $entity_name, "distance" => $distance);
            endforeach;
            Cache::put(md5($latitude.$longitude), $result2, 0.2);
            return count($result2);
        else:
            return response()->json(Cache::get(md5($latitude.$longitude)));
        endif;

    }

    public function registrarLog($latitude,$longitude,$status,$quantidade)
    {
        $log = new Log;
        $log->latitude = $latitude;
        $log->longitude = $longitude;
        $log->status = $status;
        $log->num_lojas = $quantidade;
        $log->data_hora = date('Y-m-d H:i:s');
        $log->save();
    }

}
