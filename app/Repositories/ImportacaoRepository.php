<?php

namespace App\Repositories;

use App\Model\City;
use App\Model\County;
use App\Model\State;
use App\Model\EstablishmentType;
use App\Model\OperationType;
use App\Model\Store;

class ImportacaoRepository
{
    
    public function truncateTables()
    {
        City::truncate();
        County::truncate();
        State::truncate();
        EstablishmentType::truncate();
        OperationType::truncate();
        Store::truncate();
    }

   
    public function cadastrarGenericModel($array,$table)
    {
        $result_ = [];
        $prepareSql = [];
        $res = array_unique($array);
        foreach($res as $r): 
            $prepareSql[] = array("name" => $r);
        endforeach;

        switch($table)
        {
            case 'State':
                State::insert($prepareSql);
                $result_ = State::get(['id','name'])->toArray();
                break;
            case 'OperationType':
                OperationType::insert($prepareSql);
                $result_ = OperationType::get(['id','name'])->toArray();
                break;
            case 'EstablishmentType':
                EstablishmentType::insert($prepareSql);
                $result_ = EstablishmentType::get(['id','name'])->toArray();
                break;
            default:
                break;
        }
        $result = array_column($result_,"name","id");
        return $result;
    }

    public function listarGenericModel($table)
    {
        switch($table)
        {
            case 'State':
                $result_ = State::get(['id','name'])->toArray();
                break;
            case 'OperationType':
                $result_ = OperationType::get(['id','name'])->toArray();
                break;
            case 'EstablishmentType':
                $result_ = EstablishmentType::get(['id','name'])->toArray();
                break;
            default:
                break;
        } 
        $result = array_column($result_,"name","id");
        return $result;
    }

    public function cadastrarCity($vetState,$vetCity)
    {
        $result = [];
        $new_vetState = array_flip($vetState);
        $res = array_column($vetCity, "state", "city");

        $prepareSql = [];
        foreach($res as $city => $state): 
            $prepareSql[] = array("name" => $city, 'state_id' => $new_vetState[$state]);
        endforeach;

        $new_array = array_chunk($prepareSql,9);
        foreach($new_array as $n):
            City::insert($n);
        endforeach;

        return $this->listarCity();
    }

    public function listarCity()
    {
        $result_ = City::get(['id','name'])->toArray();
        $result = array_column($result_,"name","id");
        return $result;
    }

    public function cadastrarCounty($vetCity,$vetCounty)
    {
        $result = [];
        $new_vetCity = array_flip($vetCity);
        $res = array_column($vetCounty, "city", "county");

        $prepareSql = [];
        foreach($res as $county => $city):
            $prepareSql[] = array("name" => $county, 'city_id' => $new_vetCity[$city]);
        endforeach;

        County::insert($prepareSql);
        return $this->listarCounty();
    }

    public function listarCounty()
    {
        $result_ = County::get(['id','name'])->toArray();
        $result = array_column($result_,"name","id");
        return $result;
    }

    public function cadastrarStore($sql)
    {
        Store::insert($sql);
    }

      

}
