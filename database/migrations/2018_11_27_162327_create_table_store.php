<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('license_number')->nullable();
            $table->string('entity_name',300)->nullable();
            $table->string('dba_name',300)->nullable();
            $table->integer('street_number')->nullable();
            $table->text('street_name')->nullable();
            $table->text('address_line_2')->nullable();
            $table->text('address_line_3')->nullable();
            $table->integer('zipcode')->nullable();
            $table->integer('square_footage')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('county_id')->nullable();
            $table->integer('establishmentType_id')->nullable();
            $table->integer('operationType_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store');
    }
}
